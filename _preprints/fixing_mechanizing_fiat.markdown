---
layout: default
title: "Fixing and Mechanizing the Security Proof of Fiat-Shamir with Aborts and Dilithium"
year: 2023
authors: Manuel Barbosa, Gilles Barthe, Christian Doczkal, Jelle Don, Serge Fehr, Benjamin Grégoire, Yu-Hsuan Huang, Andreas Hülsing, Yi Lee, Xiaodi Wu
website: https://eprint.iacr.org/2023/246
conference: IACR, 2023

---
