---
layout: project

project: Jasmin
website: https://github.com/jasmin-lang/jasmin.git
git: https://github.com/jasmin-lang/jasmin.git

short: >-
  Jasmin is a workbench for high-assurance and high-speed cryptography. Jasmin
  implementations aim at being efficient, safe, correct, and secure.
---
