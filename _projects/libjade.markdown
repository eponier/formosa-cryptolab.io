---
layout: project

project: Libjade
website:  https://github.com/formosa-crypto/libjade
git: https://github.com/formosa-crypto/libjade.git

short: >-
  Libjade is a cryptographic library written in jasmin, with computer-verified
  proof of correctness and security in EasyCrypt. The primary focus of libjade
  is to offer high-assurance software implementations of post-quantum crypto
  primitives.
---
