---
layout: default
title: A Fast and Verified Software Stack for Secure Function Evaluation
year: 2017
authors: José Bacelar Almeida, Manuel Barbosa, Gilles Barthe, François Dupressoir, Benjamin Grégoire, Vincent Laporte, and Vitor Pereira
website: https://eprint.iacr.org/2017/821
conference: CCS, 2017

---