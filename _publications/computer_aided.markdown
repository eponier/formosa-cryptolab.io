---
layout: default
title: "Computer-Aided Security Proofs for the Working Cryptographer"
year: 2011
authors: Gilles Barthe, Benjamin Grégoire, Sylvain Heraud, and Santiago Zanella Béguelin
website: https://hal.inria.fr/hal-01112075
conference: CRYPTO, 2011

---