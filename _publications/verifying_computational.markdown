---
layout: default
title: "Verified Computational Differential Privacy with Applications to Smart Metering"
year: 2013
authors: Gilles Barthe, George Danezis, Benjamin Grégoire, César Kunz, and Santiago Zanella-Beguelin
website: https://hal.inria.fr/hal-00935736
conference: CSF, 2013

---