---
layout: default

title: Formosa Supporters
permalink: support/
---

# Funded Projects

{% for grant in site.funding %}
- **{{ grant.name }}** ({{ grant.date | date: '%b %Y'}}—{{ grant.end | date: '%b %Y'}}): {{ grant.funder }}, {{ grant.programme }}, {{ grant.number }}<br/>
  {{ grant.content | markdownify }}
{% endfor %}

# Funders and Supporters
<br/>

{%- for supporter in site.supporters -%}
[![The {{ supporter.name }} logo](/assets/logos/{{ supporter.logo }}){: width="30%" style="margin: 10px" }]({{ supporter.website }})
{%- endfor -%}<br/>


# Affiliations
<br/>

{%- for supporter in site.institutions -%}
[![The {{ supporter.name }} logo](/assets/logos/{{ supporter.logo }}){: width="30%" style="margin: 10px" }]({{ supporter.website }})
{%- endfor -%}
